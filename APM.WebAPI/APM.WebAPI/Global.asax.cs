﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using StackExchange.Redis;
using NetMQ;

namespace APM.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        public static ConnectionMultiplexer redis;
        public static NetMQContext zctx;
        public static NetMQ.Sockets.PublisherSocket zPubSock;

        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            /* set up redis connection once, not per request!
             for any addition servers, just add them here! the multiplexer handles everything for you!
             For now, just check Docker port and make sure Kitematic doesn't change it on you...
            */
            string redisConnection = ConfigurationManager.AppSettings["redisEndPoint"];
            redis = ConnectionMultiplexer.Connect(redisConnection);

            // set up zeroMQ publisher here?
            string zmqEndPoint = ConfigurationManager.AppSettings["zmqEndPoint"];
            zctx = NetMQContext.Create();
            zPubSock = zctx.CreatePublisherSocket();
            zPubSock.Options.SendHighWatermark = 1000;
            zPubSock.Bind(zmqEndPoint);


        }

        protected void Application_End()
        {
            // trying this to release the socket, seems to jam it up on rebuilds, etc.
            if (zctx != null)
            {
                zctx.Dispose();
            }
        }
    }
}
