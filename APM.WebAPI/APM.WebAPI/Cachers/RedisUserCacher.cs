﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using DataLibrary;
using Jil;


namespace APM.WebAPI.Cachers
{
    class RedisUserCacher
    {
        private string usersKey = "Users_GetList";
        // use our connetion multiplexer "redis" set up in Global asax (only connect once!), get instance of db (cheap)
        private IDatabase db = WebApiApplication.redis.GetDatabase();

        // users get list -> no longer in use
        public User[] RetrieveUsersFromMem()
        {
            User[] users = null;
            string usersString = db.StringGet(usersKey);
            if (!string.IsNullOrEmpty(usersString))
            {
                users = JSON.Deserialize<User[]>(usersString);
                
            }
            return users;
            
        }
        // no longer needed
        public void StoreUsersInCache(IEnumerable<User> users)
        {
            string usersString = JSON.Serialize(users);
            db.StringSet(usersKey,  usersString, new TimeSpan(24, 0, 0), When.Always);            
        }

        // no longer needed
        public void ClearUsersFromCache()
        {
            db.KeyDelete(usersKey);
        }

        /// single user
        public User RetrieveUserFromMem(int UserID)
        {
            User user = null;
            string userString =  db.StringGet(GetUserKey(UserID));
            if (!string.IsNullOrEmpty(userString))
            {
                user = JSON.Deserialize<User>(userString);
            }
            return user;
        }

        private void StoreUserInCache(User user)
        {
            db.StringSet(GetUserKey(user.UserID), JSON.Serialize(user));
        }

        private string GetUserKey(int UserID)
        {
            return String.Format("UserID_{0}", UserID.ToString());
        }

        public void CacheUser(User user, bool clear = true)
        {
            // Clear Users Get List
            //if (clear)
            //{
            //    ClearUsersFromCache();
            //}
            StoreUserInCache(user);

        }
    }
}
