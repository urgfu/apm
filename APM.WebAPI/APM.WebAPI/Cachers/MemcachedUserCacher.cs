﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLibrary;
using Enyim.Caching;
using Enyim.Caching.Memcached;

namespace APM.WebAPI.Cachers
{
    public class MemcachedUserCacher
    {
        private MemcachedClient memClient = new MemcachedClient("memcached");
        private string usersKey = "Users_GetList";

        // users get list
        public User[] RetrieveUsersFromMem()
        {
            return memClient.Get(usersKey) as User[];
        }

        public void StoreUsersInCache(IEnumerable<User> users)
        {
            memClient.Store(StoreMode.Set, usersKey, users, new TimeSpan(24, 0, 0));
        }

        public void ClearUsersFromCache()
        {
            memClient.Remove(usersKey);
        }

        /// single user
        public User RetrieveUserFromMem(int UserID)
        {
            return memClient.Get(GetUserKey(UserID)) as User;
        }

        private void StoreUserInCache(User user)
        {
            memClient.Store(StoreMode.Set, GetUserKey(user.UserID), user);
        }

        private string GetUserKey(int UserID)
        {
            return String.Format("UserID_{0}", UserID.ToString());
        }

        public void CacheUser(User user, bool clear = true)
        {
            // Clear Users Get List
            if (clear)
            {
                ClearUsersFromCache();
            }
            StoreUserInCache(user);

        }

    }
}
