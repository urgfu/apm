﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using DataLibrary;
using APM.WebAPI.Cachers;
using System.Reactive;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using NetMQ;
using NetMQ.Sockets;



namespace APM.WebAPI.Controllers
{
    [EnableCorsAttribute("http://test.apmapp.com:10000", "*", "*")]
    public class UsersController : ApiController
    {
        private APMEntities db = new APMEntities();
        //private MemcachedUserCacher uc = new MemcachedUserCacher();
        //TODO: make a IUserCacher interface for both Memcahced and Redis to implement
        private RedisUserCacher uc = new RedisUserCacher();
        private PublisherSocket socket = WebApiApplication.zPubSock;

        private void SendMessage(string message)
        {
            socket.SendMore("user").Send(message);
        }
        
        // GET: api/Users
        public IEnumerable<User> GetUsers()
        {
            // no longer calls RetrieveUsers -> we don't want this to go through cache anymore, just ElasticSearch
            return RetrieveUsersFromDB();
        }

        // gets users from db, only should be called if not Memcached
        private User[] RetrieveUsersFromDB()
        {
            return db.Users.ToArray();
        }

        // Get users from cache -> if not there, hit the db and add to cache on the way back!
        // note: deprecated. now using elastic search for main list query
        private IEnumerable<User> RetrieveUsers()
        {
            User[] users = uc.RetrieveUsersFromMem();
            if (users == null)
            {
                users = RetrieveUsersFromDB();
                uc.StoreUsersInCache(users);
            }
            return users;
        }

       
        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = uc.RetrieveUserFromMem(id);
            if (user == null)
            {
                user = db.Users.Find(id);
                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    // because this user was never cached (but already in get list) and we're not making an update
                    uc.CacheUser(user, false);
                }
            }
           

            return Ok(user);
        }


        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                // publish update
                SendMessage(id.ToString());

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);
            db.SaveChanges();
            // publish update
            SendMessage(user.UserID.ToString());
            

            return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();
            // TODO:
            SendMessage(id.ToString());
            // TODO: need to remove the specific user from the cache too!
            

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserID == id) > 0;
        }
    }
}