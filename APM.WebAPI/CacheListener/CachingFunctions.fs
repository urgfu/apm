﻿module CachingFunctions

open Common.Configuration
open Common.Console
open DataLibrary
open Jil
open StackExchange.Redis
open System

// helpers -------------------------------

(* note: strings will implcitly convertto Redis Keys and Values in C# but NOT in F#
 * use op_Implicit for this sort of thing
 * ultimate could make one generic operator
 *) 
let (!!) (x:string) = RedisKey.op_Implicit(x)
let (~~) (x:string) = RedisValue.op_Implicit(x)

let usersKey = getAppSet "usersKey"

let getUserKey userID = 
    sprintf "UserID_%i" userID

let getExpiry = 
    let ts = TimeSpan(24, 0, 0)
    let expiry = new Nullable<TimeSpan>(ts)
    expiry

// caching functions ------------------------------------
let storeUsersInCache (users: User[]) (db: IDatabase) = 
    let usersString = JSON.Serialize(users)
    let expiry = getExpiry
    db.StringSet(!!usersKey, ~~usersString, expiry, When.Always) |> ignore

let storeUserInCache (user: User) (db: IDatabase) = 
    let userString = JSON.Serialize(user)
    let expiry = getExpiry
    let userKey = getUserKey user.UserID
    db.StringSet(!!userKey, ~~userString,expiry, When.Always) |> ignore

let clearUsersFromCache (db: IDatabase) = 
    db.KeyDelete(!!usersKey)

let clearUserFromCache (userID: int) (db: IDatabase) =
    let key = getUserKey userID
    db.KeyDelete(!!key)





