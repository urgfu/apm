﻿open CachingFunctions
open DataLibrary
open fszmq
open fszmq.Context
open fszmq.Socket
open Microsoft.FSharp.Linq
open StackExchange.Redis
open System
open System.Threading
open Common.Configuration
open Common.Console


(* keys *)
let redisEndPoint = getAppSet "redisEndPoint"
let zmqEndPoint = getAppSet "zmqEndPoint"

(* 0MQ helpers *)
let s_recv = Socket.recv >> System.Text.Encoding.ASCII.GetString

(* init redis *)
let redis = ConnectionMultiplexer.Connect(redisEndPoint) 
let db = redis.GetDatabase()

(* subscribe / listen *)
let listen() = 
    trace "listening..."
    use zcontext = new Context()
    use subscriber = Context.sub zcontext
    Socket.connect subscriber zmqEndPoint
    Socket.subscribe subscriber [| "user"B |]
    
    // trying to add a slight delay, I think this is creating the bad address issue?
    Thread.Sleep 1

    // listen for messages and update cache
    while true do
        let address = s_recv subscriber
        let contents = s_recv subscriber
        // contents should be user ID
        let userID = ref 0
        let success = Int32.TryParse(contents, userID)
        match success with
            | true -> trace (sprintf "[%s] %i" address !userID)
                      clearUserFromCache !userID db |> ignore
                      //clearUsersFromCache db |> ignore // no longer doing, since main list is in ElasticSearch now, not Redis
                      // let the ElasticSearch listener pick this up too and handle that separately
            | false -> logError "Bad ID"


[<EntryPoint>]
let main argv = 
    try
        listen()
    with
        | ex -> logError (ex.Message)
                trace "trying again..."
                listen() // try again!
        
    0 // return an integer exit code
