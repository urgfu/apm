﻿CREATE TABLE [dbo].[Users] (
    [UserID]       INT          IDENTITY (1, 1) NOT NULL,
    [Username]     VARCHAR (50) NOT NULL,
    [FirstName]    VARCHAR (50) NOT NULL,
    [LastName]     VARCHAR (50) NOT NULL,
    [Email]        VARCHAR (50) NULL,
    [DateCreated]  DATETIME     CONSTRAINT [DF_Users_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    INT          NULL,
    [DateModified] DATETIME     NULL,
    [ModifiedBy]   INT          NULL,
    [Archived]     BIT          CONSTRAINT [DF_Users_Archived] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

