﻿open Common.Configuration
open Common.Console
open DataLibrary
open Nest
open System
open System.Linq

// TODO: make the create and bulk inserts generic, pass in the bulk object and the index, etc.

// get Elastic Search Client
let elasticSearchEndPt = getAppSet "elasticSearchEndPt"
let uri = new Uri((elasticSearchEndPt))
let settings = new ConnectionSettings(uri, "apm")
// don't want to camel case for now, but may want to consider setting up db this way for
// future ease...
settings.SetDefaultPropertyNameInferrer(fun p -> p) |> ignore
let client = new ElasticClient(settings)

// get users
let db = new APMEntities()
let users = db.Users.ToArray()

// create the index
let createIndex() = 
    let indexSettings = new IndexSettings()
    indexSettings.NumberOfReplicas = new Nullable<int>(1) |> ignore
    indexSettings.NumberOfShards = new Nullable<int>(1) |> ignore
    client.CreateIndex(fun c -> c.Index("apm").InitializeUsing(indexSettings).AddMapping<DataLibrary.User>(fun m -> m.MapFromAttributes())
    )

let descriptor = new BulkDescriptor()

let addToBD (user: DataLibrary.User) = 
    trace user.Username
    descriptor.Index<DataLibrary.User>(fun us -> us.Document(user).Id(Convert.ToInt64(user.UserID)))
    |> ignore

let bulkInsert() = 
    users
    |> Seq.iter addToBD
    client.Bulk(descriptor)
    |> ignore

[<EntryPoint>]
let main argv = 
    try
        //createIndex() |> ignore
        trace "Inserting..."
        bulkInsert()
        trace "Done!"
    with
    | ex -> logError ex.Message


    System.Console.ReadKey() |> ignore


    0 // return an integer exit code
