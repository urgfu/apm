﻿namespace Common

module Console =

    // console writing
    let resetColor() = 
        System.Console.ForegroundColor <- System.ConsoleColor.White

    let trace (message: string) = 
        System.Console.ForegroundColor <- System.ConsoleColor.Green    
        System.Console.WriteLine message
        resetColor()

    let logError (message: string) = 
        System.Console.ForegroundColor <- System.ConsoleColor.Red
        System.Console.WriteLine message
        resetColor()

