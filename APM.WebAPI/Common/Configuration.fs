﻿namespace Common

module Configuration =

    open System.Configuration

    // app settings
    let getAppSet (key: string) = ConfigurationManager.AppSettings.[key]


