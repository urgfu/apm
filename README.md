# README #

### The project layout ###

* A basic starter template for Angular.js  / Node / Express, backed by a C# Web API with SQL Server, ZeroMQ, and Redis for caching. The goal is to build out more modules and research / implement the best features we can along the way.
* Version 0.1

### How do I get set up? ###

* APMApp is the Angular site. It requires Node and Express and Bower. I also recommend Nodemon. 
* Includes server.js. To run, use either from the command line:

```
#!bash

node server.js
#or
nodemon server.js
```


* APMAPI is the Web API. Can be easily configured with IIS. Includes a SQL DB project that you can use to create your database, and which we should keep updated as we make changes to the schema. Make sure to watch your CORS setting in the User Controller if you change ports for Node / Express server. If you hit any errors on Application_Start due to 0MQ socket, try to kill any old IIS Processes before refreshing site.

* Now using a Dockerized instance of Redis through Kitematic (RIP Memcached). Just make sure to set your ports accordingly in the app setting in the web.config of the APMAPI and the app.config of the CacheListener.

* ZeroMQ - just run the exe for the F# CacheListener to subscribe to User updates and clear those keys from the cache. The exe will just loop and listen. Should be more resilient now.


### Contribution guidelines ###

* have fun!
* next steps are really the authentication and search (would like a new subscriber to the user updates)