/**
 * Created by Alexander on 7/18/2015.
 * A directive for the main menu, to be reused across the site
 */

(function(){
    var apmMenu = function(){
        return {
            // note: best practice is to break this out into its own html
            // file and use templateURL
            restrict: 'E',
            templateUrl: './templates/menu.html'
        };
    };

    var app = angular.module("APM");
    app.directive("apmMenu", apmMenu);

}());