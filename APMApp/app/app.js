'use strict';


(function () {
    // main app
    var app = angular.module('APM', ['ngRoute', 'myApp.version']);

    // constants
    app.constant('appSettings', {apmAPI: 'http://test.apmapi.com/api/',
        elasticSearch: 'http://192.168.99.100:32769/apm/'});

    // handle routing
    app.config(function ($routeProvider) {
        $routeProvider.when('/main', {
            templateUrl: "views/main.html",
            controller: "MainCtrl"
        }).when('/users/', {
            templateUrl: "views/users/users.html",
            controller: "UserCtrl"
        }).when('/users/:userID', {
            templateUrl: "views/users/editUser.html",
            controller: "EditUserCtrl"
        })
            .otherwise({redirectTo: "/main"});
    });

}());