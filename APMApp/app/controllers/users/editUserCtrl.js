/**
 * Created by Alexander on 7/4/2015.
 */

/**
 * Created by Alexander on 6/29/2015.
 */

// todo: handle new vs update

(function () {
    var EditUserCtrl = function ($scope, $routeParams, UserFactory) {

        // get userID from route param
        var userID = $routeParams.userID;
        $scope.oldUser = {};
        $scope.user = {};

        var copyUser = function() {
            angular.copy($scope.user, $scope.oldUser);
        };

        var resetUser = function() {
            console.log('resetting...');
            angular.copy($scope.oldUser, $scope.user);
        };

        // pulls user from db
        var getUser = function (userID) {
            UserFactory.getByID(userID).then(onGetUserComplete, onGetError);
        };

        // add user to scope
        var onGetUserComplete = function (data) {
            $scope.user = data;
            copyUser();
        };

        // get user on update? why??? really just need to handle the ID
        // being returned, or just handle inserts separately
        var onUpdateUserComplete = function (data) {
            alert('User successfully saved!');
            copyUser();
            //getUser(userID);
        };

        // error message
        var onGetError = function (reason) {
            $scope.error = "Error loading user";
        };

        // error message
        var onUpdateError = function (reason) {
            $scope.error = "Error updating user";
        };

        $scope.updateUser = function (user) {
            //console.log($scope.picUpload.files);
            UserFactory.updateUser(user).then(onUpdateUserComplete, onUpdateError);
        };

        $scope.resetUser = resetUser;


        // initial load of user
        getUser(userID);

    };

    var app = angular.module("APM");
    app.controller("EditUserCtrl", EditUserCtrl);

}());
