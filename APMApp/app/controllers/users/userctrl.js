/**
 * Created by Alexander on 6/29/2015.
 */


(function () {
    var app = angular.module("APM");


    var UserCtrl = function ($scope, UserFactory) {


        var onGetUsersComplete = function (data) {
            $scope.users = data;
            //console.log(data);
        };

        var onError = function (reason) {
            $scope.error = "Error loading users";
        };

        var onSearchComplete = function(data){
            $scope.users = [];
            var hits = data.hits.hits;
            for (var i = 0; i < hits.length; i++ ){
                var hit = hits[i];
                var user = hit._source;
                user.Relevance = hit._score;
                $scope.users.push(user);
            }
        };

        $scope.searchUsers = function () {
            // basic paging
            var query = {
                "from": $scope.from,
                "size": $scope.size
            };

            // build search query
            // todo: expand, filtering, etc.
            // todo: need to pass sorting into the query
            if ($scope.search != null && $scope.search != ""){
                query.query =
                {
                    "match": {
                        "_all" : $scope.search
                    }
                };
            }

            UserFactory.searchUsers(query).then(onSearchComplete, onError);
        };


        // default sorting and paging
        $scope.userSortOrder = 'LastName';
        $scope.userSortDir = "+";
        $scope.search = "";
        $scope.from = 0;
        $scope.size = 200;

        // get users on load
        $scope.searchUsers();
        //UserFactory.getUsers().then(onGetUsersComplete, onError);

    };

    app.controller("UserCtrl", UserCtrl);

}());