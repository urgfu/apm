/**
 * Created by Alexander on 6/29/2015.
 */


(function(){

    var app = angular.module("APM");

    var MainCtrl = function($scope){
        $scope.message = "Hello, World!";
    };

    app.controller("MainCtrl", MainCtrl);


}());
