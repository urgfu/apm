/**
 * Created by Alexander on 6/29/2015.
 */

(function () {

    var UserFactory = function ($http, appSettings) {

        // endpoints
        var apmAPI = appSettings.apmAPI;
        var userSearchURL = appSettings.elasticSearch + "user/_search";

        // get all users - shouldn't really need this anymore, as we'll search instead...
        var getUsers = function () {
            return $http.get(apmAPI + "users/")
                .then(function (response) {
                    return response.data;
                });
        };

        // get users from ElasticSearch
        var searchUsers = function (query) {
            return $http.post(userSearchURL , query)
                .then(function (response){
                    return response.data;
                });
        };

        // get a user by ID
        var getByID = function (id) {
            return $http.get(apmAPI + "users/" + id)
                .then(function (response) {
                    return response.data;
                });
        };

        // updates the user
        var updateUser = function (user) {
            user.DateModified = new Date();
            return $http.put(apmAPI + "users/" + user.UserID, user)
                .then(function (response) {
                    return response.data;
                });
        };

        // public api
        return {
            getUsers: getUsers,
            getByID: getByID,
            updateUser: updateUser,
            searchUsers: searchUsers
        };
    };

    var module = angular.module("APM");
    module.factory("UserFactory", UserFactory);

}());